# suivi_projet_ENR

Script d'exploitation cartographique du tableau de suivi des projets ENR

Cz script vise à simplifier et accélerer l'exploitation géomatique du tableau de suivi des projets ENR du Loiret.

Deux couches ponctuelles sont créées :
- une couche à usage interne pour les projets qgis.
- une couche grand public pour geoide-carto. 

La couche grand public est générée par le script sur le disque geoide prod et est synchronisé automatiquement à géoidecarto. 

La mise à jour de la carte interactive ne recquiert que le lancement du script.