###############################################################
# Script d'exploitation geomatique du tableau de suivi des ENR
# 
# v1.1 : Formatage pour l'affichage dans carto 2
# V1.0 : chargement gitlab
#
###############################################################

###############################################################
# Chargement des librairies
###############################################################

library(tidyverse)
library(sf) #Package des traitements géomatiques
library(readODS) #Package de traitement des fichiers ods
library(lubridate) #Package de traitement des dates

###############################################################
# Déclaration des variables
###############################################################

# chemin d'accès
chem_tab_suivi="K:/DCDDT/02_PDD/02_ENR_H2/Suivi_dossiers-EnR_VF2.ods"
chem_commune_bdtopo="R:/BDTOPO_V3_2020/ADMINISTRATIF/COMMUNE.SHP"
chem_enr_numerise="M:/_RESEAU ENERGIE DIVERS/N_ELECTRICITE/2-Traitement/45_EnR_Projets.shp"

# chemin de résultat
chem_result="M:/_RESEAU ENERGIE DIVERS/N_ELECTRICITE/2-Traitement/R_ENR_tables_geo/"
chem_result_publication="P:/SITE_INDUSTRIEL_PRODUCTION/N_SITE_PRODUCTION_ENERGIE/"

#Nom du projet ENR
nom_shp="45_EnR_Projets_auto"
nom_shp_publication="Sites_EnR"

###############################################################
# Traitement du suivi 
###############################################################

# Chargement de la table géographique des communes
commune<-st_read(chem_commune_bdtopo)

# Chargement de la table de suivi
suivi<-read_ods(chem_tab_suivi,sheet = "Suivi_Dossiers",skip=1)

suivi_transform<-suivi%>%
  select("Etat_ avancement_projet\n(MD)",
         "Id_projet",
         "Nom_projet",
         "Porteur_projet",
         "Nb_communes_concernees\n(Affichage auto)",
         "INSEE1\n(Affichage auto)",
         "INSEE2\n(Affichage auto)",
         "INSEE3\n(Affichage auto)",
         "Type_ENR\n(MD)",
         "Surface_ha",
         "Nb_eoliennes",
         "Puissance_Mwc_MW",
         "Source_infos\n(MD)",
         "Date_1er contact _pole_EnR_DDT",
         "Date_decision",
         "Date_mise_en_service",
         "Fichier_source_\n Perimetre")%>%
  rename(id_projet="Id_projet",
         etat="Etat_ avancement_projet\n(MD)",
         pro_nom="Nom_projet",
         pro_port="Porteur_projet",
         nb_com="Nb_communes_concernees\n(Affichage auto)",
         insee="INSEE1\n(Affichage auto)",
         insee2="INSEE2\n(Affichage auto)",
         insee3="INSEE3\n(Affichage auto)",
         type="Type_ENR\n(MD)",
         surface="Surface_ha",
         nb_eol="Nb_eoliennes",
         puiss="Puissance_Mwc_MW",
         dte_1cont="Date_1er contact _pole_EnR_DDT",
         dte_decis="Date_decision",
         dte_mes="Date_mise_en_service",
         srce="Source_infos\n(MD)",
         srce_lien="Fichier_source_\n Perimetre"
  )%>%
  mutate(nb_com=as.integer(nb_com),
         insee=as.character(insee),
         insee2=as.character(insee2),
         insee3=as.character(insee3),
         surface=round(surface,2),
         nb_eol=as.integer(nb_eol),
         dte_1cont=as_date(dte_1cont,format="%d"),
         dte_decis=as_date(dte_decis,format="%d"),
         dte_mes=as_date(dte_mes,format="%d"),
         dte_edit=today())%>%
  filter(!is.na(id_projet))%>%
  arrange(id_projet,pro_nom,pro_port)


# Création des géometries ponctuelles des communes
centroid_commune<-commune%>%
  filter(INSEE_DEP=="45")%>%
  select("INSEE_COM","NOM")%>%
  rename(insee="INSEE_COM",nom="NOM")%>%
  st_centroid()


enr_com_pts<-merge(centroid_commune,suivi_transform,by="insee")%>%
  rename(geom_pts_com=geometry)

# Chargement des numérisations
enr_num<-st_read(chem_enr_numerise)%>%
  select(ID_UNIQUE,DATE_SAISI,SOURCE_INF)%>%
  filter(!is.na(ID_UNIQUE))%>%
  rename(id_projet=ID_UNIQUE,dte_num=DATE_SAISI)

enr_num_pts<-enr_num%>%
  st_centroid()%>%
  rename(geom_pts_num=geometry)

# Jointure double colonne geom
enr_pts_join<-left_join(enr_com_pts %>% as.data.frame(), 
                          enr_num_pts %>% as.data.frame(), by = "id_projet")

enr_points<-enr_pts_join%>%
  mutate(geom_pts= ifelse(st_is_empty(geom_pts_num), geom_pts_com,geom_pts_num))%>%
  select(-geom_pts_num,-geom_pts_com)%>%
  st_as_sf()%>%
  st_set_crs(2154)

enr_points_publication<-enr_pts_join%>%
  mutate(geom_pts= ifelse(st_is_empty(geom_pts_num), geom_pts_com,geom_pts_num))%>%
  mutate(dte_decis=format(dte_decis, "%d/%m/%Y"),dte_mes=format(dte_mes, "%d/%m/%Y"))%>%
  select(-geom_pts_num,-geom_pts_com)%>%
  filter(etat %in% c("En service","Accordé"))%>%
  st_as_sf()%>%
  st_set_crs(2154)%>%
  mutate(puiss = ifelse(is.na(puiss),"Non renseigné", puiss))%>%
  select(dte_edit,dte_mes,dte_decis,puiss,surface,etat,pro_port,pro_nom,type)


###############################################################
# Enregistrement du résultat
###############################################################

write_sf(enr_points,paste0(chem_result,nom_shp,"_P.shp"),overwrite=T,layer_options = c("ENCODING=UTF-8","RESIZE=YES"))
write_sf(enr_points_publication,paste0(chem_result_publication,nom_shp_publication,"_P_045.shp"),overwrite=T,layer_options = c("ENCODING=UTF-8","RESIZE=YES"))

